#![forbid(unsafe_code)]

use assert_cmd::Command;
use biscuit::{jwa::SignatureAlgorithm, jws::Secret, ClaimsSet, CompactPart, Empty, JWT};
use std::{collections::HashMap, io::Write};
use tempfile::NamedTempFile;

#[test]
fn create_jwk_and_jwt_from_it() {
    let mut cmd = Command::cargo_bin("jwtcli").unwrap();
    cmd.args(vec!["jwk"]);
    cmd.assert().success();
    let output = cmd.output().unwrap();
    let mut file = NamedTempFile::new().unwrap();
    file.write_all(&output.stdout).unwrap();

    let mut cmd = Command::cargo_bin("jwtcli").unwrap();
    cmd.args(vec!["jwt", "--jwks", file.path().to_str().unwrap()]);
    cmd.write_stdin(output.stdout);
    cmd.assert().success();
}

#[test]
fn create_jwt_from_secret_string() {
    let mut cmd = Command::cargo_bin("jwtcli").unwrap();
    cmd.args(vec!["jwt", "--secret", "secretboi"]);
    cmd.assert().success();
}

#[test]
fn exp_claim_works() {
    let mut cmd = Command::cargo_bin("jwtcli").unwrap();
    cmd.args(vec![
        "jwt",
        "--secret",
        "secretboi",
        "--claims",
        "exp=123456",
    ]);
    cmd.assert().success();
}

#[test]
fn exp_claim_fails_noninteger() {
    let mut cmd = Command::cargo_bin("jwtcli").unwrap();
    cmd.args(vec![
        "jwt",
        "--secret",
        "secretboi",
        "--claims",
        "exp=abcdef",
    ]);
    cmd.assert().failure();
}

#[test]
fn exp_default_is_set() {
    let mut cmd = Command::cargo_bin("jwtcli").unwrap();
    cmd.args(vec!["jwt", "--secret", "secretboi"]);
    let res = cmd.assert();
    cmd.assert().success();
    let op = res.get_output();
    let read_jwt =
        JWT::<ClaimsSet<HashMap<String, String>>, Empty>::from_bytes(&op.stdout).unwrap();
    let decoded = read_jwt
        .into_decoded(
            &Secret::Bytes(b"secretboi".to_vec()),
            SignatureAlgorithm::HS256,
        )
        .unwrap();
    assert!(decoded.payload().unwrap().registered.expiry.is_some());
}
