use thiserror::Error;

pub mod biscuit;
pub mod serde_json;
pub mod time;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Clone, Debug, Error, Eq, PartialEq)]
pub enum Error {
    #[error("{0}")]
    SerdeJsonError(String),
    #[error("{0}")]
    BiscuitError(String),
    #[error("Either a secret or a Jwks is required to generate a jwt, but neither were supplied.")]
    NoJwtSecretSource,
    #[error("{0} could not be set. Failure to parse \"{1}\" to an integer.")]
    CannotParseClaimValueToInt(String, String),
    #[error(transparent)]
    JwkError(#[from] crate::jwk::error::Error),
    #[error("{0}")]
    SystemTimeError(String),
    #[error("Could not deserialize Jwt token from JSON: {0}")]
    CouldNotDeserializeJwtJsonString(String),
}
