use anyhow::Error;
use jwtcli::jwt::models::{Claim, ClaimValue};
use std::time::{Duration, SystemTime};

const EXPIRY_CLAIMS_KEY: &str = "exp";
const ISSUED_AT_TIME_KEY: &str = "iat";

pub struct CliClaimsInput(pub (String, String));
impl From<(String, String)> for CliClaimsInput {
    fn from(tuple: (String, String)) -> Self {
        Self(tuple)
    }
}
impl From<CliClaimsInput> for Claim {
    fn from(tuple: CliClaimsInput) -> Self {
        Self::new(tuple.0 .0, ClaimValue::String(tuple.0 .1))
    }
}

pub fn claims_cli_inputs_to_claims(cli_input: Vec<(String, String)>) -> Vec<Claim> {
    cli_input
        .iter()
        .map(|t| CliClaimsInput::from(t.clone()).into())
        .collect()
}

pub fn add_default_claims_if_not_present(claims: &mut Vec<Claim>) -> Result<(), Error> {
    if !try_add_int_value_from_string(claims, ISSUED_AT_TIME_KEY)? {
        // Include unix time iat if iat is not set already and defaults are indicated
        let iat = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)?
            .as_secs();
        claims.push(Claim::new(
            ISSUED_AT_TIME_KEY.to_string(),
            ClaimValue::Integer(iat),
        ));
    }

    // Handle expiry time needing to be a number as well
    if !try_add_int_value_from_string(claims, EXPIRY_CLAIMS_KEY)? {
        // Include a one-year expiry if expiry is not set already and defaults are indicated
        let oneyr = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?
            + Duration::from_secs(60 * 60 * 24 * 365);
        claims.push(Claim::new(
            EXPIRY_CLAIMS_KEY.to_string(),
            ClaimValue::Integer(oneyr.as_secs()),
        ));
    }

    Ok(())
}

/// Returns a boolean indicating whether the key existed
/// Pushes a claim to `claims` with a numeric value of the pre-existing string value
fn try_add_int_value_from_string(
    claims: &mut Vec<Claim>,
    claims_key: &'static str,
) -> Result<bool, Error> {
    // Handle expiry time needing to be a number as well
    if let Some(claim) = claims.iter().find(|c| c.key() == *claims_key) {
        if let ClaimValue::String(v) = claim.value() {
            let i: u64 = v.parse().map_err(|_| {
                anyhow!(
                    "Cannot parse the value for {}: {}",
                    claims_key.to_string(),
                    v
                )
            })?;
            claims.push(Claim::new(claims_key.to_string(), ClaimValue::Integer(i)));
        };
        Ok(true)
    } else {
        Ok(false)
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn try_add_int_value_from_string__true_when_can_parse() {
        let mut claims: Vec<Claim> = vec![Claim::new(
            ISSUED_AT_TIME_KEY.to_string(),
            ClaimValue::String("1".to_string()),
        )];

        let result = try_add_int_value_from_string(&mut claims, ISSUED_AT_TIME_KEY).unwrap();

        assert!(result)
    }

    #[test]
    pub fn try_add_int_value_from_string__false_when_no_key_exists() {
        let mut claims: Vec<Claim> = vec![];

        let result = try_add_int_value_from_string(&mut claims, ISSUED_AT_TIME_KEY).unwrap();

        assert!(!result)
    }

    #[test]
    pub fn try_add_int_value_from_string__error_when_cannot_parse() {
        let mut claims: Vec<Claim> = vec![Claim::new(
            ISSUED_AT_TIME_KEY.to_string(),
            ClaimValue::String("NaN".to_string()),
        )];
        let result = try_add_int_value_from_string(&mut claims, ISSUED_AT_TIME_KEY).unwrap_err();

        assert_eq!(
            result.to_string(),
            "Cannot parse the value for iat: NaN".to_string()
        )
    }
}
