use std::convert::TryInto;

pub mod models;
use models::{Algorithm, Jwk, Jwks, KeyId};

pub mod biscuit;
use self::biscuit::{gen_octet_jwk, gen_octet_jwks};

pub mod error;
use error::Result;

pub fn generate_jwk(algorithm: Algorithm, key_id: Option<KeyId>) -> Result<Jwk> {
    gen_octet_jwk(algorithm.into(), key_id.map(|id| id.value())).try_into()
}

pub fn generate_jwks(algorithm: Algorithm, key_id: Option<KeyId>) -> Result<Jwks> {
    gen_octet_jwks(algorithm.into(), key_id.map(|id| id.value())).try_into()
}
