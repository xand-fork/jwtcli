use anyhow::Error;
use jwtcli::{
    jwk::models::{Jwk, Jwks, KeyId},
    jwt::models::{JwksPath, JwtSecret, JwtToken},
};
use std::path::PathBuf;
use structopt::StructOpt;

pub mod cli_opts;

use cli_opts::jwtcli::{
    algorithm::try_algorithm_cli_input_to_algorithm,
    claims::{add_default_claims_if_not_present, claims_cli_inputs_to_claims},
};

pub fn run() -> Result<String, Error> {
    let opt = cli_opts::Opt::from_args();
    match opt.cmd {
        cli_opts::Cmd::Jwk {
            algorithm,
            key_id,
            as_jwks,
        } => {
            if as_jwks {
                Ok(serde_json::to_string_pretty(
                    &jwks(algorithm, key_id)?.value(),
                )?)
            } else {
                Ok(jwk(algorithm, key_id)?.value())
            }
        }
        cli_opts::Cmd::Jwt {
            claims,
            secret,
            jwks,
        } => Ok(jwt(claims, secret, jwks)?.value()),
    }
}

fn jwt(
    claims: Vec<(String, String)>,
    secret: Option<String>,
    jwks: Option<PathBuf>,
) -> Result<JwtToken, Error> {
    let mut claims = claims_cli_inputs_to_claims(claims);
    add_default_claims_if_not_present(&mut claims)?;

    jwtcli::jwt::generate_jwt(claims, secret.map(JwtSecret::new), jwks.map(JwksPath::new))
        .map_err(|e| anyhow!(e.to_string()))
}

fn jwk(algorithm: String, key_id: Option<String>) -> Result<Jwk, Error> {
    jwtcli::jwk::generate_jwk(
        try_algorithm_cli_input_to_algorithm(algorithm)?,
        key_id.map(KeyId::new),
    )
    .map_err(|e| anyhow!(e.to_string()))
}

fn jwks(algorithm: String, key_id: Option<String>) -> Result<Jwks, Error> {
    jwtcli::jwk::generate_jwks(
        try_algorithm_cli_input_to_algorithm(algorithm)?,
        key_id.map(KeyId::new),
    )
    .map_err(|e| anyhow!(e.to_string()))
}
