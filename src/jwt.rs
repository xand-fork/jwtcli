use serde_json::Value;

pub mod error;
use error::{Error, Result};

pub mod models;
use models::{Claim, JwksPath, JwtSecret, JwtToken};

pub mod biscuit;
use self::biscuit::make_jwt;

use crate::jwk::biscuit::get_jwk_key;

/// Generates a JwtToken from given claims and either a JwtSecret or a JwksPath.
/// If neither secret source is supplied then generation will fail.
/// Does not include any claims, e.g. `exp`, by default.
pub fn generate_jwt(
    claims: Vec<Claim>,
    secret: Option<JwtSecret>,
    jwks: Option<JwksPath>,
) -> Result<JwtToken> {
    let secret = if let Some(secret) = secret {
        secret.value().as_bytes().to_vec()
    } else if let Some(jwks) = jwks {
        get_jwk_key(jwks.value())?
    } else {
        return Err(Error::NoJwtSecretSource);
    };
    let claims: Vec<(String, Value)> = claims
        .into_iter()
        .map(|c| (c.key(), c.value().into()))
        .collect();

    let jwt = make_jwt(claims, secret)?;

    let asval = serde_json::to_value(&jwt)?;
    let rawstr = asval
        .as_str()
        .ok_or_else(|| Error::CouldNotDeserializeJwtJsonString(asval.to_string()))?;
    Ok(JwtToken::new(rawstr.to_string()))
}
