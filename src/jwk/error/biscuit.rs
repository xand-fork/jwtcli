impl From<biscuit::errors::Error> for super::Error {
    fn from(e: biscuit::errors::Error) -> Self {
        super::Error::BiscuitError(e.to_string())
    }
}
