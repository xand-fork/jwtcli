impl From<std::io::Error> for super::Error {
    fn from(e: std::io::Error) -> Self {
        super::Error::IoError(e.to_string())
    }
}
