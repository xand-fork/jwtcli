impl From<serde_json::Error> for super::Error {
    fn from(e: serde_json::Error) -> Self {
        super::Error::SerdeJsonError(e.to_string())
    }
}
