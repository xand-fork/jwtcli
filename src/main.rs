//! A command line tool for generating JWK(S) and JWT files. Output is written to stdout.

#![forbid(unsafe_code)]

#[macro_use]
extern crate anyhow;

use anyhow::Error;

mod cli;

fn main() -> Result<(), Error> {
    let res = cli::run();
    match res {
        Ok(s) => {
            print!("{}", s);
            Ok(())
        }
        Err(e) => Err(e),
    }
}
